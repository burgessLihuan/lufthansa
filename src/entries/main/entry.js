import '../../modules/helper-modules/base';
import './style.scss';
var item_tips =$("[data-tipsjs]");
var bottoms =$("[data-bottoms='content-bottoms']");
var video_click =$("[data-click='video-click']");
var antext =$("[data-item='antext']");
  window.radio_click = true;
var objVideo=$("#video-x");
antext.animate({"margin-top":"0"},4000);
if(!$("#radio").is(':checked')&& radio_click){
    $("#radio").click();
};

var tips_dom = $("[data-item='tips']");
$("[data-click='going']").on('click',function () {
    if(radio_click){
         window.showcrossscreen = false;
        var state_arry={};
        state_arry.click = radio_click;
        window.location.href = objVideo.data('url')+ '?' + $.param(state_arry);
    }else {
        bottoms.addClass("content-bottoms-err");
    }
});
video_click.on('click',function () {
    objVideo.css("display","block");
    objVideo.get(0).play();
});
$("[data-click='xuan']").on('click',function (e) {
    var url = $(e.currentTarget).data('url');
    var state_arry={};
    state_arry.click = radio_click;
    window.location.href =  url + '?' + $.param(state_arry);

});
$("[data-clickjs]").on('click',function (e) {
    var tip_name = $(e.currentTarget).data("clickjs");
    // item_tips.show();
    $.each(item_tips,function () {
      if($(this).data("tipsjs") ==tip_name){
          $(this).show();
      }
    })
});
$("[data-click='tips-lose']").on('click',function () {
    item_tips.hide();
});
$("#radio").on('click',function(){
    if(!radio_click){
        radio_click =true;
        bottoms.addClass("opacity-all");
        bottoms.removeClass("content-bottoms-err");
    }else {
        radio_click =false;
        bottoms.removeClass("opacity-all");
        bottoms.removeClass("content-bottoms-err");
    }
});


objVideo.get(0).addEventListener('ended', function() {
    Orientation();
    objVideo.css("display","none");
    window.location.href=objVideo.data('mainurl');
});
objVideo.get(0).addEventListener('webkitendfullscreen', function(e) {
    Orientation();
    objVideo.css("display","none");
    window.location.href=objVideo.data('mainurl');
});
objVideo.get(0).addEventListener('webkitenterfullscreen', function(e) {
    Orientation();
    objVideo.css("display","none");
    window.location.href=objVideo.data('mainurl');
});

function Orientation(){
  window.showcrossscreen = true;
    //横屏
    if (window.orientation == 90 || window.orientation == -90) {
      tips_dom.addClass("crossscreen-show");
      $(".base-container").hide();
    }
}