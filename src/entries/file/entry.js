import '../../modules/helper-modules/base';
import './style.scss';
import photoclip from 'photoclip';
window.PhotoClip  = photoclip;

var from_ve =$("[data-verifi='non-null']");
var canvas = document.getElementById('canvas');
var canvas_2d = canvas.getContext('2d');
var CLICK_COLOR ="#FFFFFF";
var click_color_dom = $("[data-click='img-color']");


var pc = new PhotoClip('#clipArea', {
//		size: 260,
//		outputSize: 640,
    adaptive: ['100%', '100%'],
    file: '#file',
    view: '',
    ok: "[data-click='file-bottom']",
//		img: 'img/mm.jpg',
    loadStart: function() {
        console.log('开始读取照片');
    },
    loadComplete: function() {
        console.log('照片读取完成');
    },
    done: function(dataURL) {
        var verifarry = [];
        from_ve.each(function () {
            if($(this).val() =="" || $(this).val() ==undefined || $(this).val()==null) {
                $(this).addClass("verification-error");
                if($(this).data('tip')){
                    alert($(this).data('tip'));
                }
                $(this).focus()
                verifarry.push(1);
            }else{
                $(this).removeClass("verification-error");
            }
        });
        if(!contains(verifarry, 1)){ //返回true
            drawToCanvas(dataURL,$("input[id=keyword]").val());
        }
        // console.log(dataURL);
    },
    fail: function(msg) {
        alert('请上传图片');
    },
});

 // var newImg=document.getElementById('file-img');
 //    var newImg1=document.getElementById('new-img');
var multiple = 1;
//
var if_go_file;
var x;
var y;
$("#clipArea").on('touchstart',function(event){ //获取鼠标按下的位置
    x = event.touches[0].pageX;
    y = event.touches[0].pageY;

});
$("#clipArea").on('touchend',function(event){//鼠标释放
    var newX =event.changedTouches[0].pageX;
    var newY =event.changedTouches[0].pageY;

    if(newX== x && newY==y && if_go_file){
        $("input[type=file]").trigger("click");
    }

});
click_color_dom.on('click',function (e) {
    var item =$(e.target)
    click_color_dom.removeClass('item-after');
    item.addClass('item-after');
    if(item.hasClass("item-black")){
        CLICK_COLOR="#000000";
    }else if(item.hasClass("item-blue")){
        CLICK_COLOR="#101B48";
    }else {
        CLICK_COLOR="#FFFFFF";
    }

});
$("input[type=file]").change(function(e){
    $("#clipArea").css("z-index","1");
    if_go_file = true;

});
function drawToCanvas(imgData,word){
    var img = new Image;
    img.src = imgData;

    img.onload = function(){
       // 把 viewport 设置为 实际宽高(像素)的 4 倍
       canvas.width = 638 * multiple;
       canvas.height = 336 * multiple;
       drawImageProp(canvas_2d, img, 0, 0, canvas.width, canvas.height);
      $(".content-file").css({"background":"url(" + img.src + ") no-repeat" ,"background-size":"cover","background-position":"center"});
        mergeimg(word);
    }
}
// /**
//  * By Ken Fyrstenberg Nilsen
//  *
//  * drawImageProp(context, image [, x, y, width, height [,offsetX, offsetY]])
//  *
//  * If image and context are only arguments rectangle will equal canvas
//  */
function drawImageProp(ctx, img, x, y, w, h, offsetX, offsetY) {

  if (arguments.length === 2) {
    x = y = 0;
    w = ctx.canvas.width;
    h = ctx.canvas.height;
  }

  // default offset is center
  offsetX = typeof offsetX === "number" ? offsetX : 0.5;
  offsetY = typeof offsetY === "number" ? offsetY : 0.5;

  // keep bounds [0.0, 1.0]
  if (offsetX < 0) offsetX = 0;
  if (offsetY < 0) offsetY = 0;
  if (offsetX > 1) offsetX = 1;
  if (offsetY > 1) offsetY = 1;

  var iw = img.width,
    ih = img.height,
    r = Math.min(w / iw, h / ih),
    nw = iw * r,   // new prop. width
    nh = ih * r,   // new prop. height
    cx, cy, cw, ch, ar = 1;

  // decide which gap to fill
  if (nw < w) ar = w / nw;
  if (Math.abs(ar - 1) < 1e-14 && nh < h) ar = h / nh;  // updated
  nw *= ar;
  nh *= ar;

  // calc source rectangle
  cw = iw / (nw / w);
  ch = ih / (nh / h);

  cx = (iw - cw) * offsetX;
  cy = (ih - ch) * offsetY;

  // make sure source rectangle is valid
  if (cx < 0) cx = 0;
  if (cy < 0) cy = 0;
  if (cw > iw) cw = iw;
  if (ch > ih) ch = ih;

  // fill image in dest. rectangle
  ctx.drawImage(img, cx, cy, cw, ch,  x, y, w, h);
}
function contains(arr, obj) {
  var i = arr.length;
  while (i--) {
    if (arr[i] === obj) {
      return true;
    }
  }
  return false;
}


function mergeimg(word) {
    var wordx = 50 * multiple;
    var wordy = 100 * multiple;
    var wordlineHeight = 42 * multiple;
    var wordSize = 40 * multiple;
    canvasTextAutoLine(word,canvas,wordx,wordy,wordlineHeight, ''+wordSize+'px Arial', CLICK_COLOR);


    var hashtagx = 50 * multiple;
    var hashtagy = 180 * multiple;
    var hashtagSize = 20 * multiple;

    var text = '#现在为世界出发';
    canvas_2d.font = ''+hashtagSize+'px Arial';
    canvas_2d.fillStyle = CLICK_COLOR;
    canvas_2d.lineWidth = 1;
    canvas_2d.fillText(text,hashtagx,hashtagy);
    $("#canvasimg1").val(canvas.toDataURL());
    // newImg.src = canvas.toDataURL();
    newCanvas(canvas.toDataURL());
    }


// /*
// str:要绘制的字符串
// canvas:canvas对象
// initX:绘制字符串起始x坐标
// initY:绘制字符串起始y坐标
// lineHeight:字行高，自己定义个值即可
// */
function canvasTextAutoLine(str,canvas,initX,initY,lineHeight,font,strokeStyle){
  var ctx = canvas.getContext("2d");
  ctx.font = font;
  ctx.fillStyle = strokeStyle;
  var lineWidth = 0;
  var canvasWidth = canvas.width;
  var lastSubStrIndex= 0;
  for(let i=0;i<str.length;i++){
    lineWidth+=ctx.measureText(str[i]).width;
    if(lineWidth>canvasWidth-initX){//减去initX,防止边界出现的问题
      ctx.fillText(str.substring(lastSubStrIndex,i),initX,initY);
      initY+=lineHeight;
      lineWidth=0;
      lastSubStrIndex=i;
    }
    if(i==str.length-1){
      ctx.fillText(str.substring(lastSubStrIndex,i+1),initX,initY);
    }
  }
}
function newCanvas(imgCode) {
    canvas_2d.clearRect(0,0,canvas.width,canvas.height);
    canvas.width = 638 * multiple;
    canvas.height = 466 * multiple;
    canvas_2d.fillStyle = '#ffffff';
    canvas_2d.fillRect(0, 0, canvas.width, canvas.height);
    var imgObj = new Image();
    imgObj.src = imgCode;
    imgObj.onload = function(){
        //背景图片
        var new_height = this.height *.95;
        var new_width = this.width *.95
        var width_x =(this.width -new_width) / 2;
        canvas_2d.drawImage(this, width_x, width_x,new_width,new_height);//this即是imgObj,保持图片的原始大小：470*480
        //ctx.drawImage(this, 0, 0,1024,768);//改变图片的大小到1024*768
        var size = 90 * multiple;
        //底部二维码
        var img =  document.getElementById('code-img');
        var y = canvas.height - size - (10 * multiple); //20 = offset
        canvas_2d.drawImage(img, 0, 0, img.width, img.height, canvas.width - width_x - size, y - 20, size, size);
        //底部文字
        // var hashtagx = 50 * multiple;
        // var hashtagy = 180 * multiple;
         var hashtagSize = 40 * multiple;
        var  hashtagSize1 = 18 * multiple;

        var text = '你的态度 SHOW出来';
        canvas_2d.font = ''+hashtagSize+'px Arial';
        canvas_2d.fillStyle = '#000000';
        canvas_2d.lineWidth = 1;
        canvas_2d.fillText(text,width_x,y + size*.6);
        //
        var text = '扫码赢机票';
        canvas_2d.font = ''+hashtagSize1+'px Arial';
        canvas_2d.fillStyle = '#000000';
        canvas_2d.lineWidth = 1;
        canvas_2d.fillText(text,canvas.width - width_x - size,y + size*.8 + 15);

        // newImg1.src = canvas.toDataURL();
        $("#canvasimg2").val(canvas.toDataURL());
        $("form").submit();
    }
}
