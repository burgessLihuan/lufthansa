import '../../modules/helper-modules/base';
import './style.scss';
import 'slick-carousel/slick/slick.js';
import 'slick-carousel/slick/slick.scss';
import 'slick-carousel/slick/slick-theme.scss';

var slider =$(".content-carousel").slick({
    autoplay: true,
    autoplaySpeed: 4000,
    dots: true
});