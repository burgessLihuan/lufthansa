import '../../modules/helper-modules/base';
import './style.scss';
var from_select =$("[data-from='select']");
var from_input =$("[data-from='input']");
var from_bg =$("[data-from='bg']");
from_select.on("change",function (e) {
    var select=  $(e.target);
    var parent = select.parent();
    parent.data("selected")?form_set1(parent,select):form_set2(parent,select);
});
from_input.on('click',function (e) {
    var input=  $(e.target);
    var parent = input.parent();
    input.val("");
    parent.removeClass("verification-error");
});
function  form_set1(parent,select) {
        if(select.val()){
            parent.find(".bg-title").text(select.val());
            parent.data("val",select.val());
            parent.removeClass("verification-error");
        }else {
            parent.find(".bg-title").text( parent.data("selected"));
            parent.data("val","");
            parent.addClass("verification-error");
        }
}
function form_set2(parent,select) {
    if(select.val()){
        parent.find("span").text(select.val());
        parent.find("span").removeClass("icon");
        parent.data("val",select.val());
        parent.removeClass("verification-error");
    }else {
        parent.find("span").addClass("icon");
        parent.find("span").text("");
        parent.data("val","");
        parent.addClass("verification-error");
    }
}
$("[data-click='form']").on("click",function () {
    from_bg.each(function () {
        if($(this).data("type") == "phone"){
            var nameVal = $.trim($(this).find("input").val());
            var regName = /^1[34578]\d{9}$/;
            if(nameVal== "" || !regName.test(nameVal)){
                $(this).addClass("verification-error");
            }
            // if($(this).find("inpue").val()=="" ||$(this).find("inpue").val() ==undefined || $(this).find("inpue").val()==null){
            //     $(this).addClass("verification-error");
            // }

        }else {
            if($(this).data("val") =="" || $(this).data("val") ==undefined || $(this).data("val")==null) {
                $(this).addClass("verification-error");
                $(this).find(".bg-title").text($(this).data("selected"));
            }
        }
    });
    if($("form").find(".verification-error").length == 0) {
        $("form").submit();
    }
});