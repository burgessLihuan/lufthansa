import template from './template.hbs';
import './style.scss';
import bgUrl from '../../entries/loading/assets/bg-lan.png';

export default () => {
  return template({
    bgUrl: bgUrl
  });
}