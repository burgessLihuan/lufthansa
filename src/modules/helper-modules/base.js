import jQuery from 'jquery';
window.$ = jQuery.noConflict();
window.showcrossscreen = true;
import "../styles/main.scss";


var tips_dom = $("[data-item='tips']");
var ifor = $(".loading-show").length;
function userOrientation(){
    //竖屏
    if(window.orientation==180||window.orientation==0){
          tips_dom.removeClass("crossscreen-show");
          $(".base-container").show();
    }
    //横屏
    if(window.orientation==90||window.orientation==-90){
      if(showcrossscreen == true) {
        tips_dom.addClass("crossscreen-show");
        $(".base-container").hide();
      }
    }
}
if(!ifor){
    userOrientation();
    window.addEventListener("onorientationchange" in window ? "orientationchange" : "resize", userOrientation, false);
}


































//
// import "./layer/theme/default/layer.css";
// import "./layer/mobile/need/layer.css";
// import "./iosSelect/iosSelect.css";
// import "./nano-scroller/nanoScroller.scss";
// import "./jquery-ui-1.12.1/jquery-ui.css";
// import "./laydate/theme/default/laydate.css";
// import "./swiper/swiper.css";
//
// window.jQuery = window.$;
// import IosSelect from '../../modules/helper-modules/iosSelect/iosSelect';
// import './nano-scroller/jquery.nanoscroller';
// import "./jquery-ui-1.12.1/jquery-ui";
// import laydate from './laydate/laydate';
// import Swiper from "./swiper/swiper";
// window.laydate = laydate;
// window.Swiper = Swiper;
//
// window.IosSelect = IosSelect;
// window.validateReg = {
//   email: /^[a-z0-9]+([._\\-]*[a-z0-9])*@([a-z0-9]+[-a-z0-9]*[a-z0-9]+.){1,63}[a-z0-9]+$/,
//   mobile: /^\d{11}$/,
//   postcode: /^[1-9][0-9]{5}$/,
//   validate: function (tar, isvalidate,layer) {
//     var name = $(tar).attr("data-bind");
//     var text = $(tar).val();
//     var require = name == 'require' ? true : false;
//     if (require) {
//       if ($(tar).val().length == 0) {
//         isvalidate = false;
//         if(layer) layer.msg($(tar).data('error'));
//         return false;
//       } else {
//         isvalidate = isvalidate && true;
//       }
//     } else {
//       var reg = window.validateReg[name];
//       if (reg && !reg.test(text)) {
//         isvalidate = false;
//         if(layer) layer.alert($(tar).data('error'))
//         return false;
//       } else {
//         isvalidate = isvalidate && true;
//       }
//     }
//     return isvalidate;
//   },
//   allValidate: function (layer) {
//     var isvalidate = true;
//     $("[data-bind]").each(function (index, tar) {
//       isvalidate = window.validateReg.validate(tar, isvalidate,layer)
//     })
//     return isvalidate;
//   }
// };
// $("[data-bind]").each(function (index, tar) {
//   $(tar).on("keyup", function () {
//     window.validateReg.validate(tar, true)
//   })
// })
// laydate.path = true;
//
//
// initRootData();
//
// isTablet();
//
// initIosSelect();
//
// function initRootData() {
//   window.root = {};
//   root.IS_LOGIN = false;
//   isTablet();
// }
//
// function isTablet() {
//   return window.root.IS_TABLET = "ontouchstart" in window ? true : false;
// }
//
// $("body").click(function () {
//   if ($(this).parents(".opt-input-box-value").length == 0) {
//     $(".select_ul").addClass("is-hide");
//   }
//   if ($(this).parents(".header .information").length == 0) {
//     $(".header .search .search_result").html('');
//   }
// });
//
// function initIosSelect() {
//   $("[data-select]").each(function (index, item) {
//     let $container = $(item).closest(".opt-input-box-value");
//     $container.off();
//     $container.on('click', function (e) {
//       e.stopPropagation();
//       if (isTablet()) {
//         function getData(callback) {
//           let selectData = [];
//           $(item).siblings('.select_ul').find('li').each(function (index, item) {
//             let value = $(item).text();
//             let id = $(item).data('id') ? $(item).data('id') : index;
//             let obj = {
//               'value': value,
//               'id': id
//             };
//             if (!$(item).hasClass("is-hide")) {
//               selectData.push(obj);
//             }
//           });
//           // debugger
//           callback(selectData);
//         }
//         if( $(item).siblings('.select_ul').find('li').length==0) return false;
//         let val = $(item).attr("data-selectid") || "0";
//         let selecter = new IosSelect(1, [getData], {
//           container: ".ios-select-container",
//           title: false,
//           itemHeight: 50,
//           itemShowCount: 5,
//           oneLevelId: val,
//           callback: function (selectOneObj) {
//             $(item).text(selectOneObj.value);
//             $(item).attr('data-selectid', selectOneObj.id);
//             if (typeof window.selectClick == "function") {
//               window.selectClick($(item));
//             }
//             if (typeof window.addCheckOutType == "function") {
//               window.addCheckOutType($(item));
//             }
//             if (typeof window.addressChange == "function") {
//               window.addressChange($(item));
//             }
//             if (typeof window.areaClick == "function") {
//               window.areaClick($(item));
//             }
//           }
//         })
//         $(".ios-select-widget-box").find(".layer").append($("<div class='ios-line'></div><div class='ios-line2'></div>"))
//       } else {
//         if ($container.find(".select_ul").hasClass('is-hide')) {
//           if (parseInt($container.offset().top / $('body').height() * 100) > 65) {
//             $container.find(".select_ul").addClass('select_ul_up')
//           }
//           $container.find(".select_ul").removeClass("is-hide");
//           $container.find(".select_ul .nano").nanoScroller({ alwaysVisible: true });
//           $container.find(".select_ul li").off();
//           $container.find(".select_ul li").on("click", function (e) {
//             e.stopPropagation();
//             $container.find(".select_ul li").removeClass('blue');
//             $(this).addClass('blue');
//             $(item).text($(this).text());
//             $(item).attr('data-selectid', $(this).data('id'));
//             $container.find(".select_ul").addClass("is-hide");
//             if (typeof window.selectClick == "function") {
//               window.selectClick($(item));
//             }
//             if (typeof window.addCheckOutType == "function") {
//               window.addCheckOutType($(item));
//             }
//             if (typeof window.addressChange == "function") {
//               window.addressChange($(item));
//             }
//             if (typeof window.areaClick == "function") {
//               window.areaClick($(item));
//             }
//           })
//         } else {
//           $container.find(".select_ul").addClass("is-hide");
//         }
//       }
//     })
//   })
//   window.root.initIosSelect = initIosSelect;
// }
//
// // 函数节流
// window.throttle = function (method, context, delay) {
//   clearTimeout(method.tId);
//   method.tId = setTimeout(function () {
//     method.call(context);
//   }, delay)
// }
// window.resetTime=function () {
//    if(typeof window.header_resetTime=='function'){
//      window.header_resetTime();
//    }
// };
// //载入loading层
// window.openLoadingView = function () {
//   window.layerItem = layer.load(1, {
//     shade: [0.3, "#000"]
//   })
// }
// //消除loading层
// window.closeLoadingView = function () {
//   layer.close(window.layerItem);
// }
// //获取当天的YYYY/MM/DD格式时间
// window.getNowFormatDate = function () {
//   let date = new Date();
//   let seperator1 = "/";
//   let year = date.getFullYear();
//   let month = date.getMonth() + 1;
//   let strDate = date.getDate();
//   if (month >= 1 && month <= 9) {
//     month = "0" + month;
//   }
//   if (strDate >= 0 && strDate <= 9) {
//     strDate = "0" + strDate;
//   }
//   let currentdate = year + seperator1 + month + seperator1 + strDate;
//   return currentdate;
// }
//
// //获取url参数
// window.getUrlParam = function(key) {
//   var params = location.search.substr(1).split("&").reduce(
//     function (params, pair) {
//       pair = pair.split("=");
//       if (pair.length == 2) params[pair[0]] = decodeURIComponent(pair[1].replace(/\+/g, ' '));
//       if (pair.length == 1) params[pair[0]] = "true"
//       return params;
//     },
//     {}
//   );
//   return key ? params[key] : params
// }
//
// 点击layer弹窗以外的位置关闭弹窗
// $("body").on("click",function (e) {
//   console.log(e.target.className)
//   if(e.target.className == "layui-layer-shade") {
//     layer.closeAll();
//   }
// })

