module.exports = function (a, b, c) {
  if(typeof c!="object"){
    if(c=="price"){
      var price=a[b][c];
      price = price.toString();
      if (/[^0-9\.]/.test(price)) return "invalid value";
      price = price.replace(/^(\d*)$/, "$1.");
      price = (price + "00").replace(/(\d*\.\d\d)\d*/, "$1");
      price = price.replace(".", ",");
      var re = /(\d)(\d{3},)/;
      while (re.test(price))
        price = price.replace(re, "$1,$2");
      price = price.replace(/,(\d\d)$/, ".$1");
      return price.replace(/^\./, "0.")
    }else {
      if(c=='color'){
        var upColor=a[b].toUpperCase();
        upColor=Trim(upColor,'g');
        function Trim(str,is_global){
          var result;
          result = str.replace(/(^\s+)|(\s+$)/g,"");
          if(is_global.toLowerCase()=="g")
          {
            result = result.replace(/\s/g,"");
          }
          return result;
        }
        var colorObj = {
          "BLACK": "\u9ED1\u8272",
          "BLUE": "\u84DD\u8272",
          "BROWN": "\u68D5\u8272",
          "GREEN": "\u7EFF\u8272",
          "GREY": "\u7070\u8272",
          "ORANGE": "\u6A58\u8272",
          "PINK": "\u7C89\u8272",
          "PURPLE": "\u7D2B\u8272",
          "RED": "\u7EA2\u8272",
          "SILVER": "\u94F6\u8272",
          "WHITE": "\u767D\u8272",
          "YELLOW": "\u9EC4\u8272",
          "ROSE": "\u73AB\u7470\u8272",
          "PLAIN": "\u900F\u660E\u8272",
          "PUREHAZEL": "\u7EAF\u699B\u8272",
          "BROWN/GREY": "\u68D5\u8272/\u7070\u8272",
          "BROWN\\/GREY": "\u68D5\u8272/\u7070\u8272",
          "GREY_MIRROR_BLUE": "\u7070\u8272\u955C\u7247\u84DD\u8272\u955C\u9762\u6D82\u5C42",
          "GREY_MIRROR_SILVER": "\u7070\u8272\u955C\u7247\u94F6\u8272\u955C\u9762\u6D82\u5C42",
          "GREY_MIRROR_GREEN": "\u7070\u8272\u955C\u7247\u7EFF\u8272\u955C\u9762\u6D82\u5C42"
        };
        if(colorObj[upColor]){
          return colorObj[upColor];
        }else {
          return a[b];
        }
      }else {
        return a[b][c]
      }
    }
  }else {
    return a[b]
  }
}
