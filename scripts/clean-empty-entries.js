const fs = require('fs');
const path = require('path');

const htmlRoot = './prod/pages';
const entryRoot = './prod/entries';

fs.readdirSync(htmlRoot).forEach((page) => {
  const htmlPath = path.resolve(htmlRoot, page);
  const entryPath = path.resolve(entryRoot, page.replace('.html', '.js'));

  fs.readFile(entryPath, "utf8", (error, entryJs) => {

    if (error) {
      console.error(error);
      return;
    }

    fs.unlink(entryPath, () => {
      if (error) {
        console.error(error);
        return;
      }
    });

    fs.readFile(htmlPath, "utf8", (error, html) => {
      if (error) {
        console.error(error);
        return;
      }

      fs.writeFile(htmlPath, html.replace(`src="../entries/${page.replace('.html', '.js')}">`, `>${entryJs}`), (error) => {
        if (error) {
          console.error(error);
          return;
        }
      });
    })
  });


});



